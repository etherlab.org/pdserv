#include <iostream>

#include "../src/PThread.h"

using namespace std;

pthread::Mutex mutex;

pthread::AtomicCounter<size_t> counter;

struct mythread: pthread::Thread {
    void run() {
        ++counter;
    }
};

int main(int argc, char**)
{
    if (argc > 1) {
        // Test detach; just add any argument when calling
        size_t internalcounter = 0;
        do {
            do {
                if (internalcounter < counter + 100) {
                    break;
                }

                pthread::Thread::sleep(100);
                cerr << '+';
            } while (true);

            mythread* t = new mythread;
            t->detach();

            pthread::MutexLock lock(mutex);
            ++internalcounter;
        } while (true);
    }
    else {
        // Test joinable
        mythread *queue[100];
        size_t ptr;

        for (ptr = 0; ptr < 100; ++ptr)
            queue[ptr] = 0;

        ptr = 0;

        do {
            if (queue[ptr]) {
                queue[ptr]->join();
                delete queue[ptr];
            }

            queue[ptr++] = new mythread;
            if (++ptr == 100)
                ptr = 0;
        } while (true);
    }

    return 0;
}

