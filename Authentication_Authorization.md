Authentication and Authorization         {#authentication_authorization}
================================

PdServ uses SASL for authenticating users
and a very basic approach for authorization.

# Authentication

We recommend to configure PdServ to use SASL to authenticate
users against PAM, which is the default user management service
for Linux.
Please mind that in this case the password is transferred
as plain text, so please also set up TLS.
Using the SASL password database is discouraged,
because the passwords are stored in plaintext on the server.
There are many other authentication provider options,
for example using a LDAP server.

## Server setup

The PAM service name PdServ uses is configurable,
the default of `service_name` is `pdserv`.
Please install a PAM service file in `/etc/pam.d` with the same name
as the service. For convenience, a template file is provided in the `pam`
directory. For a quick start, just copy this file into `/etc/pam.d`.

The next step is installing the required PAM modules and start `saslauthd`,
which is platform-dependent.

Testing the SASL setup can be done with `testsaslauthd`.
Unfortunately, the utlity does not support reading the password from the command line.
```sh
$ read -s -p "Password: " PASSWORD
Password:
$ testsaslauthd -u <USERNAME> -p $PASSWORD -s pdserv
0: OK "Success."
$ unset PASSWORD
```

### OpenSuSE

Please install the `cyrus-sasl-plain` and `cyrus-sasl-saslauthd` packages.
Start and and enable saslauthd with `systemctl start saslauthd`
and `systemctl enable saslauthd`.

### Debian

Please install the `libsasl2-modules` and `sasl2-bin` packages.
Add `START=yes` to `/etc/default/saslauthd`.
Then, do `systemctl restart saslauthd` and `systemctl enable saslauthd`.

## Client Setup

Usually, installing `cyrus-sasl-plain` resp. `libsasl2-modules` is enough.
Remember to set up TLS encryption to avoid password sniffing.

# Authorization

Currently, permissions can be given in three different ways:
Per default, no authentication is required and no restictions
are imposed.
If the `guest` parameter in `pdserv.conf` is set
to `read`, all users are allowed to read signals, messages etc.
but not to write parameters. Only authenticated users are allowed
to write paramters.
If `guest` is set to `deny` instead, unauthorized access is forbidden.
Trying to read a signal or write a parameter will result in the
connection being closed.
