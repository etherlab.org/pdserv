/*****************************************************************************
 *
 *  Copyright 2023 Bjarne von Horn <vh at igh dot de>
 *
 *  This file is part of the pdserv library.
 *
 *  The pdserv library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  The pdserv library is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with the pdserv library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PDSERV_EXCEPTIONS_H
#define PDSERV_EXCEPTIONS_H

#include <cstring>
#include <stdexcept>

namespace PdServ {

class Errno : std::runtime_error
{
    int err_;

  public:
    explicit Errno(int err) : std::runtime_error(strerror(err < 0 ? -err : err)),
        err_(err < 0 ? -err : err)
    {}

    int getErrno() const noexcept { return err_; }
};

}  // namespace PdServ


#endif  // PDSERV_EXCEPTIONS_H
