/*****************************************************************************
 *
 *  $Id$
 *
 *  Copyright 2010-2019 Richard Hacker (lerichi at gmx dot net)
 *
 *  This file is part of the pdserv library.
 *
 *  The pdserv library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  The pdserv library is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with the pdserv library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "Session.h"
#include "Event.h"
#include "XmlElement.h"

using namespace MsrProto;

/////////////////////////////////////////////////////////////////////////////
bool Event::toXml(Session* session, const PdServ::EventData& eventData,
        XmlElement* parent)
{
    const PdServ::Event* event = eventData.event;

    if (event) {
        const char* tag =
            eventData.state ? getTagName(eventData.priority) : "reset";
        XmlElement msg(
                parent ? parent->createChild(tag) : session->createElement(tag));

        XmlElement::Attribute(msg, "name").setEscaped(event->path);
        if (event->nelem > 1)
            XmlElement::Attribute(msg, "index") << eventData.index;
        XmlElement::Attribute(msg, "seq") << eventData.seqNo;
        if (eventData.state)
            XmlElement::Attribute(msg, "prio")
                << getPrioInt(eventData.priority);
        XmlElement::Attribute(msg, "time") << eventData.time;
        if (eventData.state
                and event->message and event->message[eventData.index])
            XmlElement::Attribute(msg, "text")
                .setEscaped(event->message[eventData.index]);
    }

    return event;
}

/////////////////////////////////////////////////////////////////////////////
const char *Event::getTagName(PdServ::Event::Priority priority)
{
    struct Map: std::map<PdServ::Event::Priority, const char*> {
        Map() {
            const char* crit_error = "crit_error";
            const char* error = "error";
            const char* warn = "warn";
            const char* info = "info";

            insert(std::make_pair(PdServ::Event::Emergency, crit_error));
            insert(std::make_pair(PdServ::Event::Alert,    crit_error));
            insert(std::make_pair(PdServ::Event::Critical, crit_error));
            insert(std::make_pair(PdServ::Event::Error,    error));
            insert(std::make_pair(PdServ::Event::Warning,  warn));
            insert(std::make_pair(PdServ::Event::Notice,   info));
            insert(std::make_pair(PdServ::Event::Info,     info));
            insert(std::make_pair(PdServ::Event::Debug,    info));
        }
    };

    static const Map map;

    Map::const_iterator it = map.find(priority);
    return it != map.end() ? it->second : "message";
}

/////////////////////////////////////////////////////////////////////////////
const char *Event::getPrioStr(PdServ::Event::Priority priority)
{
    struct Map: std::map<PdServ::Event::Priority, const char*> {
        Map() {
            insert(std::make_pair(PdServ::Event::Emergency, "emergency"));
            insert(std::make_pair(PdServ::Event::Alert,    "alert"));
            insert(std::make_pair(PdServ::Event::Critical, "critical"));
            insert(std::make_pair(PdServ::Event::Error,    "error"));
            insert(std::make_pair(PdServ::Event::Warning,  "warning"));
            insert(std::make_pair(PdServ::Event::Notice,   "notice"));
            insert(std::make_pair(PdServ::Event::Info,     "info"));
            insert(std::make_pair(PdServ::Event::Debug,    "debug"));
        }
    };
    static const Map map;

    Map::const_iterator it = map.find(priority);
    return it != map.end() ? it->second : "unknown";
}

/////////////////////////////////////////////////////////////////////////////
int Event::getPrioInt(PdServ::Event::Priority priority)
{
    struct Map: std::map<PdServ::Event::Priority, int> {
        Map() {
            insert(std::make_pair(PdServ::Event::Emergency, 0));
            insert(std::make_pair(PdServ::Event::Alert,    1));
            insert(std::make_pair(PdServ::Event::Critical, 2));
            insert(std::make_pair(PdServ::Event::Error,    3));
            insert(std::make_pair(PdServ::Event::Warning,  4));
            insert(std::make_pair(PdServ::Event::Notice,   5));
            insert(std::make_pair(PdServ::Event::Info,     6));
            insert(std::make_pair(PdServ::Event::Debug,    7));
        }
    };
    static const Map map;

    Map::const_iterator it = map.find(priority);
    return it != map.end() ? it->second : 7;
}
