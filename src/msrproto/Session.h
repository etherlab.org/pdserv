/*****************************************************************************
 *
 *  $Id$
 *
 *  Copyright 2010 Richard Hacker (lerichi at gmx dot net)
 *
 *  This file is part of the pdserv library.
 *
 *  The pdserv library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  The pdserv library is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with the pdserv library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef MSRSESSION_H
#define MSRSESSION_H

// Version der MSR_LIBRARIES
#define _VERSION  6
#define _PATCHLEVEL 0
#define _SUBLEVEL  10


#define MSR_VERSION (((_VERSION) << 16) + ((_PATCHLEVEL) << 8) + (_SUBLEVEL))

/* List of the MSR protocol features, this server implementation supports.
 *
 * pushparameters: Parameters are sent to the client on change
 * binparameters: Parameters can be transferred in binary format
 * eventchannels: Channels can be transferred only on change
 * statistics: Display statistical information about connected clients
 * pmtime: The time of a parameter change is transferred
 * aic: Ansychronous input channels are supported
 * messages: Channels with the attribute "messagetyp" are monitored and are
 *           sent as clear text message on change (V:6.0.10)
 * polite: Server will not send any messages such as <pu> or <log> by itself
 * list: Server understands <list> command
 * login: SASL authentication is supported
 * history: the message_history command is supported
 * xsap: the xsap command is supported
 * group: the server can handle xsad/data grouping
 *        (since commit 62b47b39 from 2014, before version 3)
 */

#define MSR_FEATURES \
    "pushparameters," \
    "binparameters," \
    "eventchannels," \
    "statistics," \
    "pmtime," \
    "aic," \
    "messages," \
    "polite," \
    "list," \
    "login," \
    "history," \
    "xsap," \
    "group"


#include "../Session.h"
#include "../PThread.h"
#include "../TCP.h"
#include "XmlParser.h"
#include "XmlElement.h"

#include <vector>
#include <set>
#include <queue>
#include <cstdio>

namespace PdServ {
    class Task;
    class Parameter;
    class Variable;
    struct TaskStatistics;
    struct SessionStatistics;
}

class ParameterData;

namespace MsrProto {

class SubscriptionManager;
class Server;
class Parameter;

class Session:
    public net::TCPSession,
    public PdServ::Session {
    public:
        Session( Server *s, net::TCPServer* tcp);

        void close();

        void broadcast(Session *s, const struct timespec& ts,
                const std::string &action, const std::string &element);
        void parameterChanged(const Parameter*,
                const char* data, const struct timespec*);
        void setAIC(const Parameter* p);
        void getSessionStatistics(PdServ::SessionStatistics &stats) const;
        XmlElement createElement(const char *name);

        const struct timespec *getTaskTime(const PdServ::Task* task) const;
        const PdServ::TaskStatistics *getTaskStatistics(
                const PdServ::Task* task) const;

        double *getDouble() const {
            return &tmp.dbl;
        }

    protected:
        ~Session();
    private:

        Server * const server;

        size_t inBytes;
        size_t outBytes;

        std::ostream xmlstream;

        std::string commandId;
        uint32_t eventId;

        size_t getReceiveBufSize() const;

        // Protection for inter-session communication
        pthread::Mutex mutex;

        typedef std::set<const PdServ::Variable*> PdServVariableSet;
        PdServVariableSet knownVariables;

        // List of parameters that have changed
        typedef std::set<const Parameter*> ParameterSet;
        ParameterSet changedParameter;

        bool parameterMonitor;
        ParameterSet parameterMonitorSet;
        std::queue<ParameterData*> parameterMonitorData;

        // Asynchronous input channels.
        // These are actually parameters that are misused as input streams.
        // Parameters in this set are not announced as changed as often as
        // real parameters are.
        typedef std::set<const PdServ::Parameter*> MainParameterSet;
        MainParameterSet aic;
        size_t aicDelay;        // When 0, notify that aic's have changed

        // Broadcast list.
        typedef struct {
            struct timespec ts;
            std::string action;
            std::string message;
        } Broadcast;
        typedef std::list<const Broadcast*> BroadcastList;
        BroadcastList broadcastList;

        typedef std::vector<SubscriptionManager*> SubscriptionManagerVector;
        SubscriptionManagerVector subscriptionManager;
        const SubscriptionManager *timeTask;

        // Temporary memory space needed to handle statistic channels
        union {
            uint32_t uint32;
            double dbl;
        } mutable tmp;

        void sendGreeting();
        // Reimplemented from pthread::Thread
        void run() override;

    protected:
        void initial() override;
        void final() override;

        // Reimplemented from PdServ::Session
        ssize_t write(const void* buf, size_t len) override;
        ssize_t read(       void* buf, size_t len) override;
    private:
        std::string peerAddr(char sep) const override;
        std::string localAddr(char sep) const override;

        void processCommand(const XmlParser*);
        // Management variables
        bool p_running;
        bool writeAccess;
        bool quiet;
        bool polite;
        bool echoOn;
        std::string remoteHostName;
        std::string client;

        // Here are all the commands the MSR protocol supports
        void broadcast(const XmlParser*);
        void echo(const XmlParser*);
        void ping(const XmlParser*);
        void readChannel(const XmlParser*);
        void listDirectory(const XmlParser*);
        void readParameter(const XmlParser*);
        void readParamValues(const XmlParser*);
        void readStatistics(const XmlParser*);
        void messageHistory(const XmlParser*);
        void remoteHost(const XmlParser*);
        void writeParameter(const XmlParser*);
        void xsad(const XmlParser*);
        void xsod(const XmlParser*);
        void xsap(const XmlParser*);
        void xsop(const XmlParser*);
        void authenticate(const XmlParser*);
};

}
#endif //MSRSESSION_H
