/*****************************************************************************
 *
 *  Copyright 2021 Bjarne von Horn (vh at igh dot de)
 *
 *  This file is part of the pdserv library.
 *
 *  The pdserv library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  The pdserv library is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with the pdserv library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PDSERV_TLSSESSION_H
#define PDSERV_TLSSESSION_H

#include "TLS.h"

#include <cerrno>
#include <cstddef>
#include <cstring>
#include <memory>

struct gnutls_session_int;

namespace PdServ {

class Session;

template <typename Parent>
class TlsSession : public Parent
{
    std::unique_ptr<gnutls_session_int, TlsDeleter> session_;
    void initial() override;
    void final() override;

  public:
    template <typename... Args>
    TlsSession(Args &&...args) : Parent(std::forward<Args>(args)...)
    {}

    ssize_t write(const void *buf, size_t len) override;
    ssize_t read(void *buf, size_t len) override;
};

}  // namespace PdServ

#endif  // PDSERV_TLSSESSION_H
