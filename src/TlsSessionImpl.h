/*****************************************************************************
 *
 *  Copyright 2021 Bjarne von Horn (vh at igh dot de)
 *
 *  This file is part of the pdserv library.
 *
 *  The pdserv library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  The pdserv library is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with the pdserv library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PDSERV_TLSSESSION_IMPL_H
#define PDSERV_TLSSESSION_IMPL_H

#include "PThread.h"
#include "Session.h"
#include "TlsSession.h"

#include <cerrno>
#include <cstddef>
#include <cstring>
#include <gnutls/gnutls.h>
#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>
#include <memory>
#include <stdexcept>

template <typename Parent>
ssize_t PdServ::TlsSession<Parent>::write(const void *buf, size_t len)
{
    if (!buf or !len)
        return 0;
    ssize_t result = gnutls_record_send(session_.get(), buf, len);
    if (result < 0 and gnutls_error_is_fatal(static_cast<int>(result)))
        return 0;
    return result;
}

template <typename Parent>
ssize_t PdServ::TlsSession<Parent>::read(void *buf, size_t len)
{
    ssize_t result = gnutls_record_recv(session_.get(), buf, len);
    if (result < 0
        and (result == GNUTLS_E_AGAIN
             or !gnutls_error_is_fatal(static_cast<int>(result))))
        return -EAGAIN;
    if (result == GNUTLS_E_PREMATURE_TERMINATION
        or result == GNUTLS_E_UNEXPECTED_PACKET_LENGTH
        or result == GNUTLS_E_SESSION_EOF)
        return 0;
    return result;
}

template <typename Parent>
void PdServ::TlsSession<Parent>::initial()
{
    gnutls_session_t tls_session = nullptr;

    int result = gnutls_init(&tls_session, GNUTLS_SERVER | GNUTLS_NONBLOCK);
    if (result) {
        LOG4CPLUS_FATAL(
                PdServ::Session::log,
                LOG4CPLUS_TEXT("gnutls_init() failed: ")
                        << LOG4CPLUS_C_STR_TO_TSTRING(gnutls_strerror(result))
                        << LOG4CPLUS_TEXT(" (")
                        << LOG4CPLUS_C_STR_TO_TSTRING(
                                   gnutls_strerror_name(result))
                        << LOG4CPLUS_TEXT(")"));
        throw pthread::Thread::CancelThread();
    }
    session_.reset(tls_session);
    PdServ::Session::main->initTlsSessionData(tls_session);
    // session_ptr is more generic for verify callbacks
    gnutls_session_set_ptr(tls_session, static_cast<PdServ::Session *>(this));
    gnutls_transport_set_ptr(tls_session, this);
    gnutls_transport_set_push_function(
            tls_session,
            [](void *uptr, const void *buf, size_t count) -> ssize_t {
                auto &This  = *reinterpret_cast<TlsSession *>(uptr);
                ssize_t ans = This.Parent::write(buf, count);
                if (ans >= 0)
                    return ans;
                gnutls_transport_set_errno(
                        This.session_.get(), static_cast<int>(ans));
                return -1;
            });
    gnutls_transport_set_pull_function(
            tls_session, [](void *uptr, void *buf, size_t count) -> ssize_t {
                auto &This = *reinterpret_cast<TlsSession *>(uptr);
                // data available?
                if (!This.isPendingRead(0)) {
                    gnutls_transport_set_errno(This.session_.get(), EAGAIN);
                    return -1;
                }
                const ssize_t ans = This.Parent::read(buf, count);

                if (ans >= 0)
                    return ans;
                gnutls_transport_set_errno(
                        This.session_.get(), -static_cast<int>(ans));
                return -1;
            });

    Parent::initial();
    for (int i = 0; i < 100; ++i) {
        const int result = gnutls_handshake(session_.get());
        if (result == GNUTLS_E_SUCCESS)
            return;
        if (result == GNUTLS_E_AGAIN)
        {
            Parent::isPendingRead(40);
        }
        else if (gnutls_error_is_fatal(result)) {
            LOG4CPLUS_FATAL(
                    PdServ::Session::log,
                    LOG4CPLUS_TEXT("gnutls_handshake() failed: ")
                            << LOG4CPLUS_C_STR_TO_TSTRING(
                                       gnutls_strerror(result))
                            << LOG4CPLUS_TEXT(" (")
                            << LOG4CPLUS_C_STR_TO_TSTRING(
                                       gnutls_strerror_name(result))
                            << LOG4CPLUS_TEXT(")"));
            throw pthread::Thread::CancelThread();
        }
    }
    LOG4CPLUS_FATAL(
            PdServ::Session::log, LOG4CPLUS_TEXT("gnutls_handshake() failed"));
    throw pthread::Thread::CancelThread();
}

template <typename Parent>
void PdServ::TlsSession<Parent>::final()
{
    if (session_)
        gnutls_bye(session_.get(), GNUTLS_SHUT_RDWR);
    Parent::final();
}

#endif  // PDSERV_TLSSESSION_IMPL_H
