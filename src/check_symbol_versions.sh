#!/bin/sh

set -e

readelf --dyn-syms --wide "$1" | \
    awk '$4 == "FUNC" && $5 != "WEAK" && $7 != "UND" && $8 !~ /@LIBPDSERV/ { print "Unversioned symbol: " $8; err = 1; } END { exit err; }'
